@echo off

cd tests/test_sign_up
python -m pytest test_sign_up.py
cd ../test_sign_in
python -m pytest test_sign_in.py
cd ../test_home_page
python -m pytest test_home_page.py
cd ../test_create_transaction
python -m pytest test_create_transaction.py
cd ../test_create_bank_account
python -m pytest test_create_bank_account.py
cd ../..

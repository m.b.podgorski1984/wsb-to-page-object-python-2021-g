import pytest
from pages.sign_in_page import SignInPage
from tests.test_sign_in.data import SignInData
from tests.test_notifications.data import NotificationsData


@pytest.fixture
def notifications_tab(driver):
    sign_in_page = SignInPage(driver)
    main_page = sign_in_page.do_login(SignInData.USERNAME, SignInData.PASSWORD)
    notifications_tab = main_page.go_to_notifications_tab()

    return notifications_tab


def test_dismiss_notification(notifications_tab):
    notifications_tab.dismiss_notification(5)

    assert notifications_tab.is_at()
    #.... 

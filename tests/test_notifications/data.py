class NotificationsData:
    NOTIFICATION_1 = 'Ibrahim Dickens liked a transaction.'
    NOTIFICATION_2 = 'Kaylin Homenick received payment.'
    NOTIFICATION_3 = 'Kaylin Homenick requested payment.'
    NOTIFICATION_4 = 'Edgar Johns received payment.'
    NOTIFICATION_5 = 'Edgar Johns commented on a transaction.'
    NOTIFICATION_6 = 'Edgar Johns requested payment.'
    NOTIFICATION_7 = 'Edgar Johns received payment.'
    NOTIFICATION_8 = 'Edgar Johns requested payment.' 

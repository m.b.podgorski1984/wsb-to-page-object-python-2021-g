Diploma project on WSB 2021 
===========
This project performs autocmatic tests on web application "Cypress Real-world" using Pytest, Selenium WebDriver and Page Object pattern. 

## Prerequisites
- Git
- Terminal (cmdr)
- Python 3.8+
- Up to date Chrome

## Start the project
- Install and open application "Cypress Real-world": https://github.com/cypress-io/cypress-realworld-app
- Open terminal
- Navigate to project directory
- Create virtual environment:

```
python -m venv venv
```

- Activate virtual environment:

```
venv/Scripts/activate.bat
```

- Add dependencies:

```
pip install -r requirements.txt
```

- Run tests:

```
tests.bat
```

## References
- Pytest - https://docs.pytest.org/en/6.2.x/contents.html#
- Selenium - https://selenium-python.readthedocs.io/
- WebDriver Maneager - https://selenium-python.readthedocs.io/
- Page Objects - https://selenium-python.readthedocs.io/page-objects.html
